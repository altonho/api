//Definimos las variables globales que usaremos.
var express = require('express')
var bodyParser = require('body-parser')
var app = express()
app.use(bodyParser.json())
var requestJson = require ('request-json')
//var nuevosusuarios = require('./nuevosUsuarios.json') esto era para usar el fichero al principio. Ya no aplica al usar ahora mlab
//Definimos la URL de MLab y el apikey necesario para la conexión.
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancoasl/collections"
var apiKey = "apiKey=UM__x8_9PALCKP7cMNPysLTvfEjLDMfh"
var clienteMlab = null
// nos creamos cliente de Mlab

//Endpoint de recuperación de Usuarios lista todos los usuarios
app.get('/apitechu/v5/usuarios', function(req,res)
{
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?s={'id':1}&" + apiKey)
  clienteMlab.get('',function(err,resM,body){
    if (!err){
      res.send(body)
    }
    else {res.send("error")}
  })
})

//Ahora endpoint para que devuelva unicamente el cliente del Id que le paso, recuperando unicamente el nombre y el apellido.
app.get('/apitechu/v5/usuarios/:id', function(req,res)
{
  var id = req.params.id
  var query = 'q={"id":'+id+'}&f={"nombre":1, "apellido":1, "_id":0}&l=1&'

  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query +  apiKey)
  //console.log(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get('',function(err,resM,body){
    if (!err){
      if (body.length > 0){
      res.send(body[0])}
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
    else {res.send("error")}
  })
})

//Ahora montamos endpoint del Login usando el mlab. si el cliente existe le crea el Logged a True. si no eciste informa que el cliente no ha sido encontrado.

app.post('/apitechu/v5/login', function(req, res) {
    var email = req.headers.email
    var password = req.headers.password
    var query = 'q={"email":"' + email + '","password":"' + password + '"}'
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1) // Login ok
        {
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
          var cambio = '{"$set":{"logged":true}}'
          //var cambio = '{"$set":{"logged":true}}'
//console.log('?q={"id":' + body[0].id  + '}&' + apiKey)
//console.log(JSON.parse(cambio2))
          clienteMlab.put('?q={"id":' + body[0].id + '}&' + apiKey, JSON.parse(cambio),function(errP, resP, bodyP) {
  //          console.log(resP.statusCode)
              res.send({"login":"ok", "id":body[0].id, "nombre":body[0].nombre, "apellidos":body[0].apellido})
          })

        }
        else {
          res.status(404).send('Usuario no encontrado')
        }
      }
    })
})


//Ahora montamos endpoint del Logout usando el mlab. Verificamos que previamente tenía el Logged a true.

app.post('/apitechu/v5/logout', function(req, res) {
    var id = req.headers.id
    var query = 'q={"id":' + id + ',"logged":true}'
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1) // Estaba Logafo
        {
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
          var cambio = '{"$set":{"logged":false}}'
          //var cambio = '{"$set":{"logged":true}}'
//console.log('?q={"id":' + body[0].id  + '}&' + apiKey)
//console.log(JSON.parse(cambio2))
          clienteMlab.put('?q={"id":' + body[0].id + '}&' + apiKey, JSON.parse(cambio),function(errP, resP, bodyP) {
            console.log(resP.statusCode)
              res.send({"logout":"ok", "id":body[0].id})
          })

        }
        else {
          res.status(200).send('Usuario no logado previamente')
        }
      }
    })
})

//Ahora montamos endpoint de la consulta de cuentas de Usuario usando el mlab. Devuelve las cuentas de un cliente dado su ID

app.get('/apitechu/v5/cuentas/cliente', function(req, res)
{
   //recoge el idCliente de la cabecera
   var idcliente = req.headers.idcliente

   //construcción query para recuperar las cuentas asociadas a un ID
   var query = 'q={"idcliente":'+idcliente+'}'
   var filter = 'f={"iban":1, "_id":0}'

   //instancio la variable con la petición que voy a realizar a Mlab
   clienteMlab = requestJson.createClient(urlMlabRaiz + '/cuentas?' + query + "&" + filter + "&" + apiKey)

   //hago la petición a la API de Mlab
   clienteMlab.get('', function(errC, resC, body)
   {
     if(!errC)
     {
       //si ha encontrado ibanes asociados a un idcliente
       if (body.length > 0)
       {
             //al usuario le devuelvo por pantalla el listado de ibanes
             res.send(body)
       }
       else
       {
         res.status(404).send("Cuentas no encontradas asociadas al usuario:"+idcliente)
       }
     }
     else
     {
         res.send(errC)
     }
   })
})

/**
* Devuelve los movimientos de una cuenta dado un iban concreto
*/
app.get('/apitechu/v5/cuentas/movimientos', function(req, res){
   //recoge el iban de la cabecera
   var iban = req.headers.iban


   //construcción query para recuperar los movimientos de un IBAN
   var query = 'q={"iban":'+"'"+iban+"'"+'}'
   var filter = 'f={"movimientos":1, "_id":0}'

   //instancio la variable con la petición que voy a realizar a Mlab
   clienteMlab = requestJson.createClient(urlMlabRaiz + '/cuentas?' + query + "&" + filter + "&" + apiKey)

   //hago la petición a la API de Mlab
   clienteMlab.get('', function(errM, resM, body){
     if(!errM){
       //si ha encontrado movimientos asociados a un IBAN
       if (body.length > 0){
             //al usuario le devuelvo por pantalla el listado de ibanes
             res.send(body)
       }
       else{
         res.status(404).send("Movimientos no encontradas asociadas al IBAN:"+iban)
       }
     }
     else{
         res.send(errM)
     }
   })
})


/*
app.get('/apitechu/v3/cuentas/movimientos', function(req, res){
  //recoge el iban de la cabecera
  var iban = req.headers.iban
  //crea el array auxiliar donde guardar los movimientos
  var arrayMovimientos = []

  for (var i = 0; i < cuentas.length; i++) {
    //si se ha encontrado el iban pasado por cabecera se almacenan los movimientos
    if(cuentas[i].iban == iban){
      arrayMovimientos.push(cuentas[i].movimientos)
      break;
    }
  }

  //Si no se ha encontrado el IBAN se devuelve mensaje de error
  if(arrayMovimientos.length == 0){
    res.send("No se ha encontrado el IBAN: "+iban)
  }
  else{
    res.send(arrayMovimientos)
  }
})

*/




var port = process.env.PORT || 3000
//declaraciones para instanciar express
var fs = require('fs')
//declaracion para instanciar fs
var usuarios = require('./usuarios.json')


console.log("Hola mundo")
app.listen(port)

console.log("API escuchando en el puerto "+port)

app.get('/apitechu/v1',function(req, res)
{
  //console.log(req)
  res.send({"mensaje":"Bienvenido a mi API"})
})

app.get('/apitechu/v1/usuarios', function(req,res)
{
  res.send(usuarios)
})

app.post('/apitechu/v1/usuarios', function(req,res)
{
var nuevo = {"first_name":req.headers.first_name,
            "country":req.headers.country}
            usuarios.push(nuevo)
  console.log(req.headers)
  const datos =JSON.stringify(usuarios)

  fs.writeFile("./usuarios.json",datos, "utf8",function(err){
if (err) {
console.log (err)
}
else {
console.log("Fichero guardado")
}
})
  res.send("Alta OK")
})

app.delete('/apitechu/v1/usuarios/:id',function(req,res){
  usuarios.splice(req.params.id-1,1)
  res.send("Usuario borrado")
})


app.post('/apitechu/v1/monstruo/:p1/:p2', function(req,res){
console.log("Parametros")
console.log(req.params)
console.log("Qyery Strings")
console.log(req.query)
console.log ("Headers ")
console.log(req.headers)
console.log ("body ")
console.log(req.body)

res.send()
})

app.post('/apitechu/v2/usuarios', function(req,res)
{
var nuevo = req.body
            usuarios.push(nuevo)
  //console.log(req.headers)
  const datos =JSON.stringify(usuarios)

  fs.writeFile("./usuarios.json",datos, "utf8",function(err){
if (err) {
console.log (err)
}
else {
console.log("Fichero guardado")
}
})
  res.send("Alta OK")
})

app.post('/apitechu/v2/login', function (req,res){

var usuario = req.headers.usuario
var pasword = req.headers.pasword
var idusuario = 0
//menosuno para que no decuelva en caso de no encontrar usuario
var posicion = -1

 //recorremos la colección elemento a elemento mientras no haya login
 for (var i = 0; i < usuarios.length; i++) {
   if (usuarios[i].email == usuario && usuarios[i].password == pasword) {
     idusuario = usuarios[i].id
     usuarios[i].loged = true
     posicion=i
     break;
   }
 }

 //si hemos podido hacer login, devolvemos al json el id de usuario
 if (idusuario != 0) {
  res.send({"encontrado":usuarios[posicion].loged, "id: ": idusuario,"posicion": posicion})
  //res.send(usuarios[idusuario.loged])
  //console.log("llega aquí")
  //console.log(usuarios[idusuario-1].loged)
 }
 else{
   res.send({"encontrado": "no", "id: ": idusuario,"posicion": posicion})
 }
})

//Ahora el Logout
app.post('/apitechu/v2/logout', function (req,res){

var idusuario = req.headers.id
var usuario_logado = false

 //recorremos la colección elemento a elemento mientras no haya login
 for (var i = 0; i < usuarios.length; i++) {
   if (usuarios[i].id == idusuario && usuarios[i].loged == true) {
          usuario_logado = true
          usuarios[i].loged = false
     posicion=i
     break;
   }
 }

 //si hemos podido hacer login, devolvemos al json el id de usuario
 if (usuario_logado) {
  res.send({"logout":"si", "id: ": idusuario})
  //res.send(usuarios[idusuario.loged])
  //console.log("llega aquí")
  //console.log(usuarios[idusuario-1].loged)
 }
 else{
   res.send({"logout": "no", "msj: ": "el usuario no se habia logado"})
 }
})

/*Ahora empezamos la parte de las cuentas]*/

var cuentas = require('./cuentas.json')


app.get('/apitechu/v3/cuentas/iban', function(req,res)
{
  var arrayCuentas = []
  for (var i = 0; i < cuentas.length; i++) {
    cuentas[i]
arrayCuentas.push(cuentas[i].iban)
  }
  res.send(arrayCuentas)
})

//Para recibiendo en el header un iban encontrar los movimientos asociados

app.get('/apitechu/v3/cuentas/movimientos', function(req, res){
  //recoge el iban de la cabecera
  var iban = req.headers.iban
  //crea el array auxiliar donde guardar los movimientos
  var arrayMovimientos = []

  for (var i = 0; i < cuentas.length; i++) {
    //si se ha encontrado el iban pasado por cabecera se almacenan los movimientos
    if(cuentas[i].iban == iban){
      arrayMovimientos.push(cuentas[i].movimientos)
      break;
    }
  }

  //Si no se ha encontrado el IBAN se devuelve mensaje de error
  if(arrayMovimientos.length == 0){
    res.send("No se ha encontrado el IBAN: "+iban)
  }
  else{
    res.send(arrayMovimientos)
  }
})


//Para recibiendo en el header un id de cliente encontrar los cuentas asociadas

app.get('/apitechu/v3/cuentas/cliente', function(req, res){
//recoge el idCliente de la cabecera
var idCliente = req.headers.idcliente
//crea el array auxiliar donde guardar las cuentas
var arrayCuentasAux = []
  for (var i = 0; i < cuentas.length; i++) {
    //si se ha encontrado el idCliente pasado por cabecera se almacenan sus cuentas
    if(cuentas[i].idcliente == idCliente){
      arrayCuentasAux.push(cuentas[i].iban)
    }
  }

//Si no se ha encontrado el idCliente se devuelve mensaje de error
if(arrayCuentasAux.length == 0){
  res.send("No se ha encontrado el idcliente: "+idCliente)
  }
  else{
    res.send(arrayCuentasAux)
  }
})
